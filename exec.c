#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h> 

int main(int argc, char* argv[], char* envp[]) {
  printf("PID: %d\n", getpid());
  execv(argv[1], &argv[1]);
  printf("Wird nur im Fehlerfall ausgegeben!\n");

  return 0;
}