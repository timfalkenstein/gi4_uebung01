#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char* argv[], char* envp[]) {
  pid_t pid = fork();
  int stc;

  if (pid == 0) { // Kindprozess
    printf("[K] Meine PID: %d\n", getpid());
    execv(argv[1], &argv[1]);
    exit(1); // nur im Fehlerfall ausgefuehrt
  }

  if (pid > 0) { // Elternprozess
    printf("[E] Meine PID: %d\n", getpid());
    printf("[E] PID Kindprozess: %d\n", pid);
    wait(&stc); // aufs Kind warten

    if (WIFEXITED(stc)) {
      printf("[E] Kindprozess mit %d beendet\n", WEXITSTATUS(stc));
    }
  }

  return 0;
}