.PHONY: all default clean

default: exec fork isset myecho myenv

all: exec fork isset myecho myenv mysh

exec: exec.c

fork: fork.c

isset: isset.c

myecho: myecho.c

myenv: myenv.c

mysh: mysh.c
	$(CC) -o mysh mysh.c -I/usr/include/readline -lreadline

clean:
	rm -f exec false fork isset myecho myenv mysh skeleton true
