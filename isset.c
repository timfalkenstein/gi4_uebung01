#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[], char* envp[])
{
  char* value = NULL;
	int verbose = 0;

  if (argc > 1) {
    if (argc > 2) {
      if (strcmp(argv[1], "-v") == 0) {
				verbose = 1;
			}
    }

    value = getenv(argv[1+verbose]);
    
    if (value != NULL)
		{
      if (verbose) {
				printf("%s=%s\n", argv[1+verbose], value);
			}
			
      return 0;
    }
    else if (verbose) {
			printf("%s is NOT set!\n", argv[1+verbose]);
		}
  }

  return 1;
}