#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline.h>
#include <unistd.h>
#include <sys/wait.h>

////////////////////////////////////////////////
// Ein _Grundgeruest_ zur Loesung/Erweiterung //
// der Zusatzaufgabe "MyShell" aus Uebung 01  //
////////////////////////////////////////////////

void get_args(char** args, char* scanline) {
  int i, j, len = strlen(scanline);

  args[0] = scanline;

  for (i = 0, j = 1; i < len; i++) {
    if (scanline[i] == ' ') {
      scanline[i] = '\0';
      args[j] = &(scanline[i + 1]);
      j++;
    }
  }

  args[j] = NULL;
}

int main(int argc, char* argv[], char* envp[]) {
  int pid;
  char* scanline;
  char* mypath;
  char* myprompt;
  char* args[256];
  char path[256];

  mypath = getenv("MYPATH");
  if (mypath == NULL) mypath = "/bin";

  myprompt = getenv("MYPROMPT");
  if (myprompt == NULL) myprompt = "> ";

  do {
    scanline = readline(myprompt);
    if(scanline == NULL) { printf("exit\n"); exit(0); }

    add_history(scanline);
    get_args(args, scanline);

    if (strcmp(args[0], "exit") == 0) exit(0);

    pid = fork();

    if (pid == 0) {
      sprintf(path, "%s/%s", mypath, args[0]);
      printf("Ich bin der Kindprozess, starte: %s\n", path);
      execv(path, args);
      exit(-1);
    }

    if (pid > 0) {
      printf("Ich bin der Elternprozess, das Kind ist %d.\n", pid);
      wait(NULL);
    }
  } while(1);
}
